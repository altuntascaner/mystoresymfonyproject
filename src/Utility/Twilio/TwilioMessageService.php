<?php
	
	namespace App\Utility\Twilio;
	
	use Twilio\Rest\Client;
	
	class TwilioMessageService 
	{		
		
		public function sendMessage($messageBody,$from,$to): ?string
		{	
			$sid = "AC8450b5db3a8484df19e37fe40b31257a";
			$token = "40a1e15fe4b9f63d1ee965b07c4a6c2a";
			
			$client = new Client($sid,$token);

			$message = $client->messages
			                  ->create("+15005550010", // to
			                           array("from" => "+15005550006", "body" => $messageBody)
			                  );

	 		return $message->sid;
		}
	}
?>