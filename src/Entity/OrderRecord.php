<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRecordRepository")
 */
class OrderRecord
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $createdBy;

    /**
     * @ORM\Column(type="string")
     */
    private $createdDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $isApproved;

    /**
     * @ORM\Column(type="integer")
     */
    private $isPreOrder;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getCreatedDate(): ?string
    {
        return $this->createdDate;
    }

    public function setCreatedDate(string $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    public function getIsApproved(): ?int
    {
        return $this->isApproved;
    }

    public function setIsApproved(int $isApproved): self
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    public function getIsPreOrder(): ?int
    {
        return $this->isPreOrder;
    }

    public function setIsPreOrder(int $isPreOrder)
    {
        $this->isPreOrder = $isPreOrder;

        return $this;
    }
}
