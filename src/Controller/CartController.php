<?php

	namespace App\Controller;

	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\Routing\Annotation\Route;

	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\EmailType;

	//use App\Controller\Product;
	use App\Repository\ProductRepository;
	use App\Repository\PreorderRepository;

	use App\Entity\CartItem;
	use App\Entity\Product;
	use App\Entity\PreorderForm;

	class CartController extends AbstractController
	{	
		/**
	     * @Route("/cart", name="cart")
	     */    
	    public function index()
	    {
	    	$returnArray = array();
	    	$cartItems = array();

			$session =  $this->get('session');

			if($session->has('cartItemsList')){ //check if any cart exist if false create one
	    		$cartItems = $session->get('cartItemsList');
		    }

			if(count($cartItems) > 0){
			    $productRepository = $this->getDoctrine()
										->getManager()
								        ->getRepository(Product::class);
 
 				$tempArray = array();
 				$total = 0;
 				$subTotal = 0;

				foreach ($cartItems as $i => $item) { 
	        		$productName = $productRepository->find($item->getProductId())
										        	 ->getName();

					$subTotal = $item->getQuantity() * 	$item->getUnitPrice();

					array_push($tempArray, array(	"productId" => $item->getProductId(),
													"productName" => $productName, 
													"quantity" => $item->getQuantity(), 
													"unitPrice" => $item->getUnitPrice(),
													"subTotal" => $subTotal ));

					$total += $subTotal;								        	 	
	        	}				        

	        	$returnArray["items"] = $tempArray;
	        	$returnArray["total"] = $total;
			}
	        
	        return $this->render('cart/index.html.twig', [
            	'cartInfo' => $returnArray
        	]);

        	//return new Response(var_dump($returnArray));
	    }

	    /**
	     * @Route("/cart/addProductToCart/{productId}/{quantity}", name="addProductToCart")
	    */
	    public function addProductToCart($productId, $quantity){

	    	$returnMessage = "error";

			try {

		    	$addNewItem = false;//flag to control if a new addition is necessary

		    	$session =  $this->get('session');

		    	if(! $session->has('cartItemsList')){ //check if any cart exist if false create one
		    		$session->set('cartItemsList', array());
		    		$addNewItem = true;
		    	}
				
				$currentCartItems = $session->get('cartItemsList');
				$product =  $this->getDoctrine()
									->getManager()
							        ->getRepository(Product::class)
							        ->find($productId);

		    	if($addNewItem == false){
			    	$filteredCartItems = array_filter( $currentCartItems, 
			    										  function($v, $k) use ($productId){
															    return $v->getProductId() == $productId;
														  }, 
														   ARRAY_FILTER_USE_BOTH
														); // if some quantity of the product has already been added just increase its quantity

			    	if(count($filteredCartItems) > 0){
			    		$existingCartItem = array_values($filteredCartItems)[0];

			    		$currentQuantityInCart = $existingCartItem->getQuantity();

			    		if($currentQuantityInCart + $quantity > $product->getStockCount()){
			    			$returnMessage = "There is/are ".$currentQuantityInCart." of the product in your cart already. You can add only ".($product->getStockCount() - $currentQuantityInCart)." more";
			    		}
			    		else{
			    			$existingCartItem->setQuantity($existingCartItem->getQuantity() + $quantity);
			    			$returnMessage = 'success';
			    		}
			    	}
			    	else{
			    		$addNewItem = true;
			    	}
				}

				if($addNewItem){ //add new item
					$cartItem = new CartItem();

					$cartItem->setProductId($productId);
					$cartItem->setQuantity($quantity);

					$price =$product->getPrice(); // get price of the product

					$cartItem->setUnitPrice($price);

					array_push($currentCartItems,$cartItem); // add new cart item	

					$returnMessage = 'success';	        

				}

				$session->set('cartItemsList', $currentCartItems); // reset session item				
			
			} 
			catch (Exception $e) {
			    $returnMessage = 'error: '.$e->getMessage();
			}

			return new Response($returnMessage);
	    }

		/**
	     * @Route("/cart/changeCartItemQuantity/{itemIndex}/{quantity}", name="changeCartItemQuantity")
	    */
	    public function changeCartItemQuantity($itemIndex,$quantity){
	    	$returnText = "error";

	    	$cartItems = array();

			$session =  $this->get('session');

			if($session->has('cartItemsList')){ //check if any cart exist 
	    		$cartItems = $session->get('cartItemsList');
		    }

		    if(count($cartItems) > 0){
		    	$cartItem = $cartItems[$itemIndex];//get cart item

		    	$product =  $this->getDoctrine()
									->getManager()
							        ->getRepository(Product::class)
							        ->find($cartItem->getProductId());//get related product

				if($quantity > $product->getStockCount()){//check quantity against stock
					$returnText = "Insufficient stock";	
				}
				else{
					$cartItem->setQuantity($quantity);//set new quantity

					$returnText = "success";
				}
		    }

		    return new Response($returnText);
	    }

	    /**
	     * @Route("/cart/removeCartItem/{itemIndex}", name="removeCartItem")
	    */
	    public function removeCartItem($itemIndex){
	    	$returnText = "error";

	    	$cartItems = array();

			$session =  $this->get('session');

			if($session->has('cartItemsList')){ //check if any cart exist 
	    		$cartItems = $session->get('cartItemsList');
		    }

		    if(count($cartItems) > 0){
		    	unset($cartItems[$itemIndex]);//unset current cart item

				$session->set('cartItemsList', array_values($cartItems)); // reset session item

				$returnText = "success";				
		    }

		    return new Response($returnText);
	    }

	    /**
	     * @Route("/cart/getCartItemsCount", name="getCartItemsCount")
	    */
	    public function getCartItemsCount(){
			$session =  $this->get('session');

	    	$count = 0;

	    	if($session->has('cartItemsList')){ //check if any cart exist 
	    		$count = count($session->get('cartItemsList'));
		    }

		    return new Response($count);
	    }

	    /**
	     * @Route("/cart/buyCart", name="buyCart")
	    */
	    public function buyCart(){
			$session =  $this->get('session');

	    	$count = 0;

	    	if($session->has('cartItemsList')){ //check if any cart exist 
	    		$count = count($session->get('cartItemsList'));
		    }

		    return new Response($count);
	    }
	}

?>