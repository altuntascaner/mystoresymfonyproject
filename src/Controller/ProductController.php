<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index()
    {
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
        ]);
    }

/**
     * @Route("/product/showProducts", name="showProducts")
     */

    public function showProducts(){
		$products = $this->getDoctrine()->getRepository(Product::class)
									->findAll();		
		
		
		return $this->render('product/index.html.twig', [
            'products' => $products
        ]);
	}
    
}
