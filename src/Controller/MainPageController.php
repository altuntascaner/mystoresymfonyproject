<?php

	namespace App\Controller;

	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\Routing\Annotation\Route;


	class MainPageController extends AbstractController
	{	    
	    public function index()
	    {
	    	$session = $this->get('session');

	    	$userType = $session->get('userType');

	    	if($userType == "admin"){
	    		return $this->redirectToRoute('showOrders');	
	    	}
	    	else{
	        	return $this->redirectToRoute('showProducts');
	    	}
	    }

	}

?>