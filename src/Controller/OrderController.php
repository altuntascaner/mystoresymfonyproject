<?php

	namespace App\Controller;

	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\Routing\Annotation\Route;

	use Symfony\Component\Serializer\Serializer;
	use Symfony\Component\Serializer\Encoder\JsonEncoder;
	use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

	//use App\Controller\Product;
	use App\Repository\ProductRepository;
	use App\Repository\PreorderRepository;

	use App\Entity\CartItem;
	use App\Entity\Product;
	use App\Entity\OrderRecord;
	use App\Entity\PreorderForm;
	
	use App\Utility\Twilio\TwilioMessageService;

	class OrderController extends AbstractController
	{
		/**
	     * @Route("/order", name="showOrders")
	     */    
	    public function index()
		{
			$session = $this->get('session');
			$userType = $session->get('userType');

			$returnArray = array();

			$entityManger = $this->getDoctrine()
								 ->getManager();
			
			$filter = "";

			if($userType == "user"){
				$filter = "created_by = 1";
			}

			$orders = $entityManger->getRepository(OrderRecord::class)
					->findAllForView($filter);

			if(count($orders) >0 ){

				$cartItemRepository = $entityManger->getRepository(CartItem::class);

				$productRepository = $entityManger->getRepository(Product::class);	

				foreach ($orders as $i => $order) {					

					$cartItems = $cartItemRepository->findBy(["orderId"=>$order["id"]]);

					$total = 0;
					$subTotal = 0;

					$cartItemsList = array();
					$tempArray = array();

					foreach ($cartItems as $j => $cartItem) {
						$productName = $productRepository->find($cartItem->getProductId())->getName();
						$quantity = $cartItem->getQuantity();
						$unitPrice = $cartItem->getUnitPrice();

						$tempArray["productName"] = $productName;
						$tempArray["quantity"] = $quantity;
						$tempArray["unitPrice"] = $unitPrice;

						$subTotal = $quantity * $unitPrice;

						$tempArray["subTotal"] = $subTotal;

						array_push($cartItemsList,$tempArray);

						$total += $subTotal;
					}

					$preorderForm = $entityManger->getRepository(PreorderForm::class)
												 ->findBy(["orderId"=>$order["id"]]);

					$returnArray[$order["id"]] = [
													 "orderInfo" => $order,
													 "cartItems" => $cartItemsList,
													 "total" =>$total,
													 "preorder" =>(count($preorderForm) > 0 ? $preorderForm[0] : "")
													];						
				}		
			}

	    	return $this->render('orderRecord/index.html.twig', [
            	'orders' => $returnArray
        	]);

		}

		/**
	     * @Route("/order/completeOrder", name="completeOrder")
	     */
	     public function completeOrder(Request $request){
			try {
				$entityManager = $this->getDoctrine()->getManager();

				$cartItems = array();

				$session =  $this->get('session');

				if($session->has('cartItemsList')){ //check if any cart exist
		    		$cartItems = $session->get('cartItemsList');
			    }

				if(count($cartItems) > 0){
				    
					$isPreOrder = $request->request->get('isPreorder');

				    $order = new OrderRecord();//create set order object

				   	$order->setCreatedBy(1); 
				   	$order->setCreatedDate(date('YmdHis'));
				   	$order->setIsApproved(0);
					$order->setIsPreOrder($isPreOrder== "true"? 1 : 0);

					$entityManager->persist($order);//save orderrecord
					$entityManager->flush(); 				

					$productRepository = $entityManager->getRepository(Product::class);

					foreach ($cartItems as $i => $item) { //adjust product stock count and then update
						$item->setOrderId($order->getId());
						$entityManager->persist($item);  

						$product = $productRepository->find($item->getProductId());	
						$product->setStockCount($product->getStockCount() - $item->getQuantity());
						$entityManager->persist($product);					
		        	}

		        	if ($isPreOrder=="true"){
						$encoders = array(new JsonEncoder()); // to deserialize form data got as json
						$normalizers = array(new ObjectNormalizer());
						$serializer = new Serializer($normalizers, $encoders);

						$formObjectJson = $request->request->get('formObject');
						$preorderForm = $serializer->deserialize($formObjectJson, PreorderForm::class, 'json');//create object
			        
						$preorderForm->setOrderId($order->getId());
						$entityManager->persist($preorderForm);		        	
					}

					$entityManager->flush();

		        	$session->set('cartItemsList',array());

		        	return new Response("success");
				}
			} catch (Exception $e) {
				return new Response('error:'.$e->getMessage());
			}
		}

		/**
	     * @Route("/order/setPreorder/{orderId}/{operation}", name="setPreorder")
	     */    
	    public function setPreorder($orderId,$operation)
		{
			try {
				$entityManager = $this->getDoctrine()
									->getManager();

				$orderRecord = $entityManager->getRepository(OrderRecord::class)							        
							        			->find($orderId);//get record

				$orderRecord->setIsApproved($operation);

				$entityManager->flush();

				$messageService = new TwilioMessageService();
				$messageService->sendMessage("Your preorder is ".($operation == 1 ? "approved.":"rejected."),"","");

				return new Response("success");

			} catch (Exception $e) {
				return new Response('error:'.$e->getMessage());
			}
		}
	}
?>