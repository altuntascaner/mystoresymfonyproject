<?php

	namespace App\Controller;

	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\Routing\Annotation\Route;

	class LoginController extends AbstractController
	{
		public function index(){
			$session =  $this->get('session');

		    $session->clear();

			return $this->render('login/index.html.twig');
		}

		/**
         * @Route("/login/{userType}", name="asdasdas")
	     */   
	    public function login($userType)
		{			
			$session =  $this->get('session');

		    $session->set('userType', $userType);

		    return $this->redirectToRoute('homepage');
		}

	}
?>