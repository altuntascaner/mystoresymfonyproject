<?php

namespace App\Repository;

use App\Entity\OrderRecord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OrderRecord|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderRecord|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderRecord[]    findAll()
 * @method OrderRecord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRecordRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OrderRecord::class);
    }

    public function findAllForView($filterString): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT p.*,'testUser' userName, DATE_FORMAT(STR_TO_DATE(p.created_date,'%Y%m%d%H%i%s'), '%d.%m.%Y %H:%i:%s') formattedDate FROM order_record p
            ".($filterString == "" ? "": " where " . $filterString)."
             ORDER BY p.created_date DESC
            ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
