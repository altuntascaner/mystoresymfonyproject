<?php

namespace App\Repository;

use App\Entity\PreorderForm;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PreorderForm|null find($id, $lockMode = null, $lockVersion = null)
 * @method PreorderForm|null findOneBy(array $criteria, array $orderBy = null)
 * @method PreorderForm[]    findAll()
 * @method PreorderForm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreorderFormRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PreorderForm::class);
    }

    // /**
    //  * @return PreorderForm[] Returns an array of PreorderForm objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PreorderForm
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
