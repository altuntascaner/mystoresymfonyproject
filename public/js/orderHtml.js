$(document).on('click','#btnApprovePreorder,#btnRejectPreorder',function(){
			var clickedButton = $(this); 
			var orderId = clickedButton.closest('.recordContainer').find('.orderRecordContainer .orderId').html();
			var operation = (clickedButton.attr('id') == 'btnApprovePreorder' ? 1 : 2);
			
			$.ajax(
				{  
		           url: '/order/setPreorder/'+orderId+'/'+operation,  
		           type: 'POST',
		           dataType: 'text',  
		           async: true,
		           success: function(result) {  

		           		if(result == "success"){
		           			var addclass = (clickedButton.attr('id') == 'btnApprovePreorder' ? 'approved' : 'rejected');

		           			clickedButton.closest('.recordContainer').find('.orderRecordContainer').addClass(addclass);
		           			clickedButton.closest('.recordContainer').find('.divPreorderUser').addClass(addclass);
		           			clickedButton.closest('.recordContainer').find('.orderRecordContainer .divPreorderStatus').html('Preorder '+ addclass);

		           			clickedButton.parent().remove();
		           		}

		           		alert(result);		           		
		           }  
	           }
	        );
		});