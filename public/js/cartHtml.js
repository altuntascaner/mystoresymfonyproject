$(document).on('change','.quantityInp',function(){
			var thisInput = $(this);
			var cartItem = thisInput.closest('.cartItemContainer');
			var cartItemIndex = cartItem.index();			
			var quantity = parseInt(thisInput.val());
			 
			if (quantity <= 0 || isNaN(quantity) || thisInput.val().indexOf('.') >= 0){
				alert('Invalid quantity');
				thisInput.val(thisInput.attr('preValue'));
			} 
			else{
				$.ajax(
					{  
			           url: 'cart/changeCartItemQuantity/'+cartItemIndex+'/'+quantity,  
			           type: 'POST',
			           dataType: 'text',  
			           async: true,
			           success: function(result) {  

			           		if(result == "success"){
			           			thisInput.attr('preValue',quantity);
			           			var unitPrice = parseInt(cartItem.find('.unitPrice').html().trim());
			           			var newSubTotal = unitPrice*quantity;

			           			cartItem.find('.subTotal').html(newSubTotal);
			           			
			           			var newTotal = 0;

			           			$('.cartItemExpense .subTotal').each(function(){
			           				newTotal += parseInt($(this).html().trim());
			           			});

			           			$('.cartTotalDiv .total').html(newTotal);
			           		} 
			           		else{
			           			thisInput.val(thisInput.attr('preValue'));
			           			alert(result);
			           		}
			           }  
		           }
		        );
	        }
		});

		$(document).on('click','.removeCartItem',function(){

			var cartItem = $(this).closest('.cartItemContainer');
			var cartItemIndex = cartItem.index();
			
			$.ajax(
				{  
		           url: 'cart/removeCartItem/'+cartItemIndex,  
		           type: 'POST',
		           dataType: 'text',  
		           async: true,
		           success: function(result) {  

		           		if(result == "success"){
		           			cartItem.remove();
		           			
		           			var newTotal = 0;

		           			$('.cartItemExpense .subTotal').each(function(){
		           				newTotal += parseInt($(this).html().trim());
		           			});

		           			$('.cartTotalDiv .total').html(newTotal);

		           			if(newTotal == 0){
		           				$('.cartItemsListContainer').parent().html('There is no items in your cart');
		           			}
		           		} 
		           		else{
		           			alert(result);
		           		}

		           		refreshCartItemCount();
		           }  
	           }
	        );
	        
		});

		$(document).on('click','#btnBuyCart',function(){
			
			if($('#chkPreorder').prop('checked')){
				var phone = $('#preorderFormPhone').val();
				var regEx = /^[0-9]{10}$/g;

				if(regEx.test(phone) == false){
					alert('Phone number is invalid.');
					return false;
				}

				 var formObject = { name : $("#preorderFormName").val(),
									surname	:  $("#preorderFormSurname").val(),
									email :	   $("#preorderFormMail").val(),
									phoneNumber	: $("#preorderFormPhone").val()
								  };
			}


			$.ajax(
				{  
		           url: '/order/completeOrder',  
		           type: 'POST',
		           data: {isPreorder: $('#chkPreorder').prop('checked'), formObject : JSON.stringify(formObject)},
		           dataType: 'text',  
		           async: true,
		           success: function(result) {  
						
						refreshCartItemCount();

		           		alert(result);

		           		if(result == "success"){
		           			window.location = 'product/showProducts';
		           		}		           		
		           }  
	           }
	        );
	        
		});

		$(document).on('change','#chkPreorder',function(){
			if($(this).prop('checked')){
				$('.preorderForm').show();
			}
			else{
				$('.preorderForm').hide();
			}
		});