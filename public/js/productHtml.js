$(document).on('click','.btnAddToCart',function(){

			var product = $(this).closest('.productContainer');
			var productId = product.find('.productId').html().trim();
			var quantity = parseInt(product.find('.quantityInp').val());
			var quantityMin = parseInt(product.find('.quantityInp').attr('min'));
			var quantityMax = parseInt(product.find('.quantityInp').attr('max'));
			 
			if (quantity <= 0 || isNaN(quantity) || product.find('.quantityInp').val().indexOf('.') >= 0){
				alert('Invalid quantity');
				product.find('.quantityInp').val(1);
			} 
			else if(quantity < quantityMin || quantity > quantityMax){
				alert('Insufficient stock');
				product.find('.quantityInp').val(1);
			}
			else{
				$.ajax(
					{  
			           url: '/cart/addProductToCart/'+productId+'/'+quantity,  
			           type: 'POST',
			           dataType: 'text',  
			           async: true,
			           success: function(result) {  
			           		
			           		alert(result);

			           		if(result == "success"){		           			

			           			refreshCartItemCount();
			           		}
			           		else{
			           			product.find('.quantityInp').val(1);
			           		} 
			           }  
		           }
		        );
	        }
		});