$(document).ready(function(){
				$('.menuLinks .activeMenuLink').removeClass('activeMenuLink');
				$('.menuLinks a[href="'+location.pathname+'"]').addClass('activeMenuLink');

				refreshCartItemCount();
			}); 

        	$(document).on('click','.menuLinks a',function(){
        		if($(this).hasClass('activeMenuLink')){
        			return false;
        		}    			
        	});

        	function refreshCartItemCount(){
        		$.ajax(
					{  
			           url: '/cart/getCartItemsCount',  
			           type: 'POST',
			           dataType: 'text',  
			           async: true,
			           success: function(result) {  

			           		if(result == "0"){
			           			$('.cartItemCountMain').hide();	
			           		} 
			           		else{
			           			$('.cartItemCountMain').html(result);
        						$('.cartItemCountMain').show();		
			           		}
			           }  
		           }
		        );
        	}